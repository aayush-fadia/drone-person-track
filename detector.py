import sys
import torch
sys.path.append('pytorch-YOLOv4')
torch.cuda.device(1)
from tool.darknet2pytorch import Darknet as DarknetYolov4
import cv2
from tool.utils import plot_boxes_cv2
from tool.torch_utils import do_detect
from itertools import count

use_cuda = torch.cuda.is_available()
cfg_v4_person = 'yolov4-custom.cfg'
weight_v4_person = 'yolov4-custom_best.weights'

m_person = DarknetYolov4(cfg_v4_person)
m_person.load_weights(weight_v4_person)
num_classes = m_person.num_classes

class_to_detect = ['person']
all_class_names = ['person','vehicle']
print('Loading weights from %s... Done!' % (weight_v4_person))
confidence_vehicle = 0.05

if use_cuda:
    m_person.cuda()


def get_boxes(frame, confidence_person=0.1):
    sized = cv2.resize(frame, (m_person.width, m_person.height))
    sized = cv2.cvtColor(sized, cv2.COLOR_BGR2RGB)
    boxes = do_detect(m_person, sized, confidence_person, 0.1, use_cuda)
    return boxes


def plot_boxes(frame, boxes):
    result_img, _, _, _ = plot_boxes_cv2(frame, boxes[0], classes_to_detect=class_to_detect,
                                         fontScale=0.5,
                                         thick=2, savename=False, class_names=all_class_names)
    return result_img
import time
cv2.namedWindow("result",cv2.WINDOW_NORMAL)
if __name__ == '__main__':

    cap = cv2.VideoCapture('AMBUS.MOV')
    # cap = cv2.VideoCapture('test_video.avi')
    cap.set(3, 1280)
    cap.set(4, 720)
    print("Starting Detection...")

    for _ in count(1):
        ret, img = cap.read()
        if not ret:
            break
        s = time.time()
        boxes = get_boxes(img)
        # print(boxes)
        result = plot_boxes(img, boxes)
        print(1/(time.time() - s))

        cv2.imshow("result", result)
        cv2.waitKey(1)
    cv2.destroyAllWindows()
