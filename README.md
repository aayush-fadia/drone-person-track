# Drone Person Tracking

## How To Run

### Selective Multi-Tracking using Mouse Clicks
* Run the code `python3 mouse_tracking_detection.py -vid <optional_video_path> -s <int> -pc <float>`.
* The arguments are:
    * `-vid`: Path for video file `default = 'test_video.avi'`
    * `-s`: Skip interval for running YOLO `default = 100`
    * `-pc`: Confidence Threshold for "person" class `default = 0.1`
* No objects are tracked prior to selection i.e. detection runs on each frame before a selection is made.
* Object selection can simply be done by left-clicking on/near the object.
* Press `q` to terminate.
* Results can be found [here](https://drive.google.com/file/d/12ZhL-ACAuOSL45akzf2XaPlyC6sUkIDp/view?usp=sharing).

### Simple Object Detection
* Run the code `python3 main.py <optional_video_path>` for detection.
* All the objects in a frame are detected and bounding boxes are displayed.


## Installation Instructions

### Steps to run
* Clone this repo `git clone https://gitlab.com/aayush-fadia/drone-person-track.git`
* Change into project dir `cd drone-person-track`
* Create a virtual environment using venv or virtualenv (optional)
* Install dependencies `pip3 install -r requirements.txt`	

### For Jetson TX2 specific commands
* Run `bash start_ngrok.sh` on Ubuntu system (amd64) to give us the remote access
* Install pytorch using this [link](https://forums.developer.nvidia.com/t/pytorch-for-jetson-nano-version-1-6-0-now-available/72048).

