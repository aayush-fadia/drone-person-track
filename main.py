#!/usr/bin/env python

import argparse

import cv2

from detector import get_boxes, plot_boxes

parser = argparse.ArgumentParser()
parser.add_argument("vid", nargs='*', default='test_video.avi', type=str, help="Input Video")
args = parser.parse_args()
cap = cv2.VideoCapture(args.vid)
while cap.isOpened():
    ret, frame = cap.read()
    boxes = get_boxes(frame)
    for box in boxes[0]:
        print(box[6])
    result = plot_boxes(frame, boxes)
    cv2.imshow("Frame", result)
    cv2.waitKey(1)
cv2.destroyAllWindows()
