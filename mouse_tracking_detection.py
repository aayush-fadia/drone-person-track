from decimal import *
import math
import sys
import torch
sys.path.append('yolov3_detector')
sys.path.append('pytorch-YOLOv4')
from tool.darknet2pytorch import Darknet as DarknetYolov4
import cv2
from tqdm.auto import tqdm
from tool.utils import plot_boxes_cv2
from tool.torch_utils import do_detect
from itertools import count
import numpy as np
import imutils
import random
import time
import argparse
from detector import get_boxes

use_cuda = torch.cuda.is_available()                               
cfg_v4_veh = 'yolov4-custom.cfg'
weight_v4_veh = 'yolov4-custom_best.weights'

m_vehicle = DarknetYolov4(cfg_v4_veh)
m_vehicle.load_weights(weight_v4_veh)
num_classes = m_vehicle.num_classes

class_names_veh = ['person']


if use_cuda:
    m_vehicle.cuda()

parser = argparse.ArgumentParser()
parser.add_argument("-vid", default='test_video.avi', type=str, help="Input Video")
parser.add_argument("-s", default=100, type=int, help="Skip Interval")
parser.add_argument("-pc", default=0.1, type=float, help="Person Confidence")
args = parser.parse_args()


global pts
Boxes_to_track = None
# pts to store the mouse click coordinates 
pts = []
# select becomes True only when user clicks(left button down)
select = False
first_select = False # To detect first click, ie to prohibit tracking before first click.
#set "interval" as per your need
interval = args.s
confidence_person = args.pc                      

# Function for obtaining the mouse click coordinates
def selection(event, x, y, flags, param):
    global select
    global first_select
    if event == cv2.EVENT_LBUTTONDOWN:
        pts.append((x,y))
        select = True
        first_select = True

# Tracker for the user selected persons
trackers_selected = cv2.MultiTracker_create()

vs = cv2.VideoCapture(args.vid)
cv2.namedWindow('Frame',cv2.WINDOW_NORMAL)
frame_number = 0
fps = 0

while vs.isOpened():
    start_time = Decimal(time.time())
    ret, frame = vs.read()
    (H,W) = frame.shape[:2]
    cv2.imshow("Frame", frame)
    cv2.setMouseCallback("Frame", selection)
    # Taking bounding boxes from yolo whenever an interval passes or user clicks or before first click
    if frame_number % interval == 0 or (select is True) or (first_select is False):   
        pre_boxes = get_boxes(frame, confidence_person)[0]                  
        p_boxes = []
        for i, box in enumerate(pre_boxes):
            if box[-1] == 0:
                box = box[:4] * np.array([W, H, W, H])
                (centerX, centerY, width, height) = box.astype("int")
                x = int(centerX - width/2)
                y = int(centerY - height/2)
                box = (x,y,width,height)
                p_boxes.append(box)
            boxes = p_boxes
            for (x, y, w, h) in boxes:
                cv2.rectangle(frame, (x,y), (x+w, y+h), (0,255,0), 2)
        if select is True:
            boxes_to_track = boxes
        if Boxes_to_track is not None and select is not True:
            b = []
            for i, box in enumerate(Boxes_to_track):
                box = np.linalg.norm(np.array(boxes)-np.array(Boxes_to_track[i]), axis=1)
                distances = sorted(box)
                b.append(boxes[np.where(box == distances[0])[0][0]])
            Boxes_to_track = b
        

    if select is True:
        b = []
        centerX = np.array(boxes_to_track)[:,0] + np.array(boxes_to_track)[:,2]/2
        centerY = np.array(boxes_to_track)[:,1] + np.array(boxes_to_track)[:,2]/2
        centerX = np.expand_dims(centerX, axis = 1)
        centerY = np.expand_dims(centerY, axis = 1)
        coords = np.concatenate((centerX, centerY), axis = 1)
        if np.array(pts).shape[0] > 1:
            temp1 = np.zeros((np.array(coords).shape[0], np.array(pts).shape[0]))
            temp2 = np.zeros((np.array(coords).shape[0], np.array(pts).shape[0]))
        else:
            temp1 = np.zeros(np.array(coords).shape[0])
            temp1 = np.expand_dims(temp1, axis=1)
            temp2 = np.zeros(np.array(coords).shape[0])
            temp2 = np.expand_dims(temp2, axis=1)
        for i in range(len(pts)):
            temp1[:, i] = np.linalg.norm(np.array(coords)-np.array(pts[i]), axis=1)
            temp2[:, i] = np.linalg.norm(np.array(coords)-np.array(pts[i]), axis=1)
        temp2.sort(axis = 0)
        distances = temp2

        # If the nearest distance between the click coordinates and centers of bounding boxes is more then 200 units then we don't consider the click as valid 
        if distances[0][-1]>200:          
            print("Please click near the object to be tracked!!")
            pts.remove(pts[-1])
        else:
            # Correcting the tracker using yolo
            for i in range(distances.shape[1]):
                b.append(boxes_to_track[np.where(temp1 == distances[0, i])[0][0]])
            Boxes_to_track = b
            # Adding the recently clicked person to the tracker
            box = Boxes_to_track[-1]
            box = np.asarray(box)
            tracker = cv2.TrackerCSRT_create()
            (x,y,w,h) = box.astype("int")
            trackers_selected.add(tracker, frame, (x,y,w,h))
        
        select = False


    if first_select:
        trackers = trackers_selected
        (success, boxes_to_track) = trackers.update(frame)
        if success:
            for i, box in enumerate(boxes_to_track):
                (x,y,w,h) = [int(v) for v in box]
                cv2.rectangle(frame, (x,y), (x+w, y+h), (0,255,0), 2)
                cv2.putText(frame, str(i), (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), thickness=3)
    end_time = Decimal(time.time())
    frame_time = end_time - start_time
    fps = 1 / frame_time
    cv2.putText(frame, f"FPS: {fps:.2}", (1600, 50), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 0, 255), thickness=3)
    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1)

    # If "q" key is pressed then program terminates
    if key == ord("q"):
        break

    frame_number += 1


cv2.destroyAllWindows()