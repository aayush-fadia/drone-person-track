# INCOMPLETE
from time import time
import argparse

import cv2

from detector import get_boxes


def box_dist(b1, b2):
    return ((b1[0] - b2[0]) ** 2) + ((b1[1] - b2[1]) ** 2)


parser = argparse.ArgumentParser()
parser.add_argument("vid", nargs='*', default='test_video.avi', type=str, help="Input Video")
args = parser.parse_args()
cap = cv2.VideoCapture(args.vid)
tracker = cv2.TrackerCSRT_create()
box_to_track = None
frame_number = 0
start_time = time()
fps = 0
while cap.isOpened():
    ret, frame = cap.read()
    if frame_number % 100 == 0:
        pre_boxes = get_boxes(frame)[0]
        p_boxes = []
        for i, box in enumerate(pre_boxes):
            if box[-1] == 0:
                (x, y, w, h) = box[:4]
                x = int(x * frame.shape[1])
                y = int(y * frame.shape[0])
                w = int(w * frame.shape[1])
                h = int(h * frame.shape[0])
                x = int(x - (w / 2))
                y = int(y - (h / 2))
                p_boxes.append((x, y, w, h))
        mindist = float('inf')
        if box_to_track is not None:
            for p_box in p_boxes:
                cdist = box_dist(p_box, box_to_track)
                if cdist < mindist:
                    mindist = cdist
                    box_to_track = p_box
        else:
            for i, box_to_draw in enumerate(p_boxes):
                x, y, w, h = box_to_draw
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 0), 5)
                cv2.putText(frame, str(i), (x, y), cv2.FONT_HERSHEY_SIMPLEX, 3, (0, 0, 0), thickness=5)
            cv2.imshow("Frame", frame)
            cv2.waitKey(0)
            to_track = input("Person To Track?")
            box_to_track = p_boxes[int(to_track)]
            start_time = time()
        tracker.init(frame, box_to_track)
    else:
        (success, box_to_track) = tracker.update(frame)
        if success:
            (x, y, w, h) = [int(v) for v in box_to_track]
            cv2.putText(frame, str(fps), (100, 100), cv2.FONT_HERSHEY_SIMPLEX, 3, (0, 0, 0), thickness=5)
            cv2.rectangle(frame, (x, y), (x + w, y + h),
                          (0, 255, 0), 2)
    cv2.imshow("Frame", frame)
    cv2.waitKey(1)
    frame_number += 1
    end_time = time()
    fps = frame_number / (end_time - start_time)

cv2.destroyAllWindows()
